﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calc
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                //needs to be able to add
                //subtract
                //div
                //multiply
                Console.WriteLine("Input first number");
                string numOne = Console.ReadLine();
                Console.WriteLine("Input Second number");
                string numTwo = Console.ReadLine();
                Console.WriteLine("Input operator");
                string op = Console.ReadLine();

                try
                {
                    decimal converedNumOne = Convert.ToDecimal(numOne);
                    decimal converedNumTwo = Convert.ToDecimal(numTwo);
                    decimal result = CalcResult(converedNumOne, converedNumTwo, op);
                    Console.WriteLine("Your result is " + result.ToString());
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
        public static decimal CalcResult(decimal numOne, decimal numTwo, string op)
        {
            decimal result = 0;
            switch (op)
            {
                case ("add"):
                    result = numOne + numTwo;
                    break;
                case ("sub"):
                    result = numOne - numTwo;
                    break;
                case ("mult"):
                    result = numOne * numTwo;
                    break;
                case ("div"):
                    if (numTwo == 0)
                    {
                        Console.WriteLine("Can not divide by 0!!!");
                        break;
                    }
                    result = numOne / numTwo;
                    break;
                default:
                    Console.WriteLine("Invalid Input"); break;
            }

            return result;
        }
    }
}

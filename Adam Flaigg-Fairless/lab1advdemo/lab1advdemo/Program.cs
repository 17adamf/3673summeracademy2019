using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1advdemo
{
    public class GlobalValues
    {
        public string userInput { get; set; }
        public string[] userInputArray { get; set; }
        public int inputLength { get; set; }

    }
    public class Program
    {
        public static GlobalValues globals = new GlobalValues();
        public static void Main(string[] args)
        {
            //take user input
            globals.userInput = Console.ReadLine();
            //output user input
            Console.WriteLine(globals.userInput);
            printTotal();
            printLoop();
            secretCheck();
            pymMaker();
            printSlow();

            //user choice
            while (true)
            {
                Console.WriteLine("\n1 = Print Total\n" +
                    "2 = Print Loop\n" +
                    "3 = Secret Check\n" +
                    "4 = Pyramid Maker\n" +
                    "5 = Print Slow\n");
                
                string userChoice = Console.ReadLine();
                
                switch (userChoice)
                {
                    case ("1"):
                        printTotal();
                        break;
                    case ("2"):
                        printLoop();
                        break;
                    case ("3"):
                        secretCheck();
                        break;
                    case ("4"):
                        pymMaker();
                        break;
                    case ("5"):
                        printSlow();
                        break;
                    default:
                        break;
                }
            }
            



            //print total words and store that information in a var
            void printTotal()
            {
                globals.userInputArray = globals.userInput.Split(' ');
                globals.inputLength = globals.userInputArray.Length;
                Console.WriteLine(globals.inputLength.ToString());
            }


            //-Run a for loop that prints the user input for the same # of times as the previous variable
            //i = i + 1
            //i+=1
            //i++
            void printLoop()
            {
                for (int i = 0; i < globals.inputLength; i++)
                {
                    Console.WriteLine(globals.userInput + i.ToString());
                }
            }




            // -Do an if check to see if the input contains a secret word of your choice, and print to the console that the secret word was found
            void secretCheck()
            {
                foreach (string temp in globals.userInputArray)
                {
                    if (temp == "secret")
                    {
                        Console.WriteLine("You found the secret word");
                    }
                }
            }
            






            // -Make a pyramid out of the * character (without manually printing each line)
            void pymMaker()
            {
                for (int i = 0; i < globals.inputLength; i++)
                {
                    string pymChar = "*";
                    for (int y = 0; y < i; y++)
                    {
                        Console.Write(pymChar);
                    }
                    Console.WriteLine();
                }
            }
            

            //-Print each character in the input individually slowly (I'll show an example)
            void printSlow()
            {
                foreach (char temp in globals.userInput)
                {
                    Console.Write(temp);
                    System.Threading.Thread.Sleep(250);
                }
            }
            

            Console.ReadKey();

            /* -At the end, prompt the user to do any of the above stuff again (Hint: menu system with a switch case)
             */
        }


    }
}

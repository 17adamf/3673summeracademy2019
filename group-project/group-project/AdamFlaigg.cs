﻿/*
* Name: Adam Flaigg-Fairless
* Date:	6/24/19
* Instructor: Adam Flaigg-Fairless
* Class Description: Displays a popup with the current time
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace group_project
{
    public class AdamFlaigg
    {

        public void popupTimeBox()
        {
            DateTime currentTime = System.DateTime.Now;
            MessageBox.Show("The Current Date & Time is " + currentTime.ToString(), "Current Date & Time: Adam Flaigg-Fairless", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }
    }
}

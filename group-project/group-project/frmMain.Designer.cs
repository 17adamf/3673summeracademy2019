namespace group_project
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblWelcome = new System.Windows.Forms.Label();
            this.btnAdam = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblWelcome
            // 
            this.lblWelcome.AutoSize = true;
            this.lblWelcome.Location = new System.Drawing.Point(12, 9);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(251, 65);
            this.lblWelcome.TabIndex = 0;
            this.lblWelcome.Text = "Welcome to the Group Project\r\n\r\nEach person will get a button on the main page, \r" +
    "\nthat will reference their class file and do the method \r\nthat is included in th" +
    "eir class.";
            // 
            // btnAdam
            // 
            this.btnAdam.Location = new System.Drawing.Point(15, 110);
            this.btnAdam.Name = "btnAdam";
            this.btnAdam.Size = new System.Drawing.Size(75, 23);
            this.btnAdam.TabIndex = 1;
            this.btnAdam.Text = "Adam";
            this.btnAdam.UseVisualStyleBackColor = true;
            this.btnAdam.Click += new System.EventHandler(this.btnAdam_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(467, 548);
            this.Controls.Add(this.btnAdam);
            this.Controls.Add(this.lblWelcome);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "frmMain";
            this.Text = "Group Project";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.Button btnAdam;
    }
}


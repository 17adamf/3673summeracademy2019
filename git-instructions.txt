If you're reading this, that means you've successfully cloned this repo to your local machine. 
Now that you've got that, we're gonna copy our project we worked on last time in C#, and paste it into a new folder for each person.
For any future programming projects, we will use this repo.

Now that we're at this step, we're going to go to the "group-project" folder and open the group-project.sln file.

For this next part, we will each make a dev branch from the master branch under our names with the format firstnameLastInitial-dev (adamF-dev)
Inside this project, we're going to each make a class file that prints out our name (or whatever fuction you want to add) when a fuction in called. It will follow the naming format of FirstnameLastName (AdamFlaigg) Something simple to become familiar with collaborative programming.
Make sure at the top of your class you have a block comment that contains the following information:

/*
* Name: NAME
* Date:	X/X/XX
* Instructor: Adam Flaigg-Fairless
* Class Description: Explain what your class file does
*/




After this, commit your changes to your branch and merge back into the master branch.

I'll be showing you how this all works on my end, as I'll be following along with everyone else.
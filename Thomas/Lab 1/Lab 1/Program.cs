﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_1
{
    public class GlobalValues
    {
        public string userInput { get; set; }
        public string[] userInputArry { get; set; }
        public int inputLength { get; set; }
    }
    public class Program
    {
        public static GlobalValues globals = new GlobalValues();
        public static void Main(string[] args)
        {
            //take user input
            globals.userInput = Console.ReadLine();
            //output user input
            Console.WriteLine(globals.userInput);
            printTotal();
            printLoop();
            secretCheck();
            pymMaker();
            printSlow();

            //user choice
            while (true)
            {
                Console.WriteLine("\n1 = Print Total\n" +
                    "2 = Print Loop\n" +
                    "3 = Secret Check\n" +
                    "4 = Pyramid Maker\n" +
                    "5 = Print Slow\n");
                string userChoice = Console.ReadLine();
                
                switch (userChoice)
                {
                    case ("1"):
                        printTotal();
                        break;
                    case ("2"):
                        printLoop();
                        break;
                    case ("3"):
                        secretCheck();
                        break;
                    case ("4"):
                        pymMaker();
                        break;
                    case ("5"):
                        printSlow();
                        break;
                    default:
                        break;


                }
            }
            

            
            //print total words and store that information in a variable
            void printTotal()
            {
                globals.userInputArry = globals.userInput.Split(' ');
                //Stores amount of words
                globals.inputLength = globals.userInputArry.Length;
                Console.WriteLine(globals.inputLength.ToString());
            }
           


            //runs a for lop that prints the user input for the same # of words in the user inputs sentence
            // i++ is equal to i = i+1
            void printLoop()
            {
                for (int i = 0; i < globals.inputLength; i++)
                {
                    Console.WriteLine(globals.userInput + i.ToString());
                }

            }



            /* Do a check to see if the user input contains a secret word of your choice,
              and print to the console that the secret word was found */
            void secretCheck()
            {

                foreach (string temp in globals.userInputArry)
                {
                    if (temp == "cookie")
                    {
                        Console.WriteLine("No!! My Cookie!!!");
                    }


                }
            }
           

            // Make a pyramid out of the * character 
            void pymMaker()
            {
                for (int i = 0; i < globals.inputLength; i++)
                {
                    string pymChar = "$";
                    for (int y = 0; y < i; y++)
                    {
                        Console.Write(pymChar);
                    }
                    Console.WriteLine();
                }
               

            }
            void printSlow()
            {
                // Print each character in the input individually slowly
                foreach (char temp in globals.userInput)
                {
                    Console.Write(temp);
                    System.Threading.Thread.Sleep(250);
                }
                Console.ReadLine();
            }
        }
    }
}

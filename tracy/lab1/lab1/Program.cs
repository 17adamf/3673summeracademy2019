﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1
{
    public class GlobalValues
    {
        public string userInput { get; set; }
        public string[] userInputArray { get; set; }
        public int inputLength { get; set; }
    }



    public class Program
    {
        public static GlobalValues globals = new GlobalValues();
        public static void Main(string[] args)

        {

            //take user input


            globals.userInput = Console.ReadLine();

            //output user input

            Console.WriteLine(globals.userInput);
            printTotal();
            printLoop();
            secretCheck();
            pymMaker();
            printSlow();

            // user choice
            while (true)
            {
                Console.WriteLine("1 = Print total\n" +
                    "2 = Print Loop\n" +
                    "3 = secret Check\n" +
                    "4 = pym Maker\n" +
                    "5 = print Slow\n");
              
                string userChoice = Console.ReadLine();
                switch (userChoice)
                {
                    case ("1"):
                        printTotal();
                        break;
                    case ("2"):
                        printLoop();
                        break;
                    case ("3"):
                        secretCheck();
                        break;
                    case ("4"):
                        pymMaker();
                        break;
                    case ("5"):
                        printSlow();
                        break;
                    default:
                        break;

                }
            }
            



            void printTotal()
            {
                //print total words and store that information in a variable

                globals.userInputArray = globals.userInput.Split(' ');

                globals.inputLength = globals.userInputArray.Length;

                Console.WriteLine(globals.inputLength.ToString());
            }

            //run a for loop that prints the user input for the same # of times as the previous variable
            void printLoop()
            {
                for (int i = 0; i < globals.inputLength; i++)
                {
                    Console.WriteLine(globals.userInput + i.ToString());
                }
            }




            // do and if check to see if the input contains a secret word of your choice, and print to the console that the secret word was found
            void secretCheck()
            {
                foreach (string temp in globals.userInputArray)

                {
                    if (temp == "chocolate")
                    {
                        Console.WriteLine("I love Chocolate");

                    }

                }
            }





            //make a pyrimid out of the  * character( without manally printing each line)
            void pymMaker()
            {
                for (int i = 0; i < globals.inputLength; i++)
                {
                    string pmyChar = "*";

                    for (int y = 0; y < i; y++)
                    {
                        Console.Write(pmyChar);


                    }
                    Console.WriteLine();
                }
            }

            // print each character in the input individually slowly
            void printSlow()
            {

                foreach (char temp in globals.userInput)
                {
                    Console.Write(temp);
                    System.Threading.Thread.Sleep(250);


                }


            }


            Console.ReadKey();


            // at the end it will prompt the user to do
        }

    }
}
